import { useContext } from "react";
import About from "../components/about/About";
import Education from "../components/education/Education";
import Experience from "../components/experience/Experience";
import Navigation from "../components/navigation";
import Projects from "../components/projects/Projects";
import { NavigationContext } from "../contest/NavigationContext";
import { motion } from "framer-motion";
import Skills from "../components/skills/Skills";

export default function Home() {

  const { activeLink, setActiveLink } = useContext(NavigationContext);

  let component = <About />

  if (activeLink == 'projects') {
    component = <Projects />
  } else if (activeLink == 'education') {
    component = <Education />
  } else if (activeLink == 'experience') {
    component = <Experience />
  } else if (activeLink == 'skills') {
    component = <Skills />
  }

  return (
    <div>
      <Navigation />
      <div style={{ margin: '20px 40px' }}>
        {component}
      </div>
    </div>
  )
}
