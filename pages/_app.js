import '../styles/globals.css'
import { NextUIProvider, createTheme } from '@nextui-org/react';
import Head from 'next/head';
import { useState } from 'react';
import { NavigationContext } from '../contest/NavigationContext';
import useDarkMode from 'use-dark-mode';

function MyApp({ Component, pageProps }) {

  const [activeLink, setActiveLink] = useState('about');

  const lightTheme = createTheme({
    type: 'light',
    theme: {
    }
  })

  const darkTheme = createTheme({
    type: 'dark',
    theme: {
    }
  });

  const darkMode = useDarkMode(false);

  return <NextUIProvider theme={darkMode.value ? darkTheme : lightTheme}>
    <Head>
      <link rel="preconnect" href="https://fonts.googleapis.com" />
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    </Head>
    <NavigationContext.Provider value={{ activeLink, setActiveLink }}>
      <Component {...pageProps} />
    </NavigationContext.Provider>

  </NextUIProvider>
}

export default MyApp
