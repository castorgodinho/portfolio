import React, { useState } from 'react'
import { motion } from "framer-motion";
import { Image, Card } from '@nextui-org/react'
import styles from './education.module.css'


const Education = () => {

    const [animating, setAnimation] = useState(false);

    return (
        <motion.div initial={{
            opacity: 0
        }}
            animate={{
                opacity: 1
            }}
            style={{ padding: '0px 20px', marginTop: '50px' }} >
            <Card className={styles.education} isHoverable variant="bordered" css={{ mw: "400px" }}>
                <Card.Body>
                    <div >
                        <div className={styles.logo}>
                            <Image className={styles.image} objectFit="contain" css={{ height: '150px' }} src='gu.png' />
                        </div>
                        <div >
                            <p className={styles.degree}>Master in Information Technology (I.T)</p>
                            <p>Chowgule College of Art and Science - Goa University</p>
                            <p>2015-2017</p>
                            <p>CGPA: 9.05/10</p>
                        </div>
                    </div>
                </Card.Body>
            </Card>
            <Card className={styles.education} isHoverable variant="bordered" css={{ mw: "400px" }}>
                <Card.Body>
                    <div >
                        <div className={styles.logo}>
                            <Image showSkeleton className={styles.image} objectFit="contain" css={{ height: '150px' }} src='gu.png' height="100" />
                        </div>
                        <div className={styles.ed_desc}>
                            <p className={styles.degree}>Bachelors in Computer Science</p>
                            <p>Chowgule College of Art and Science - Goa University</p>
                            <p>2012-2015</p>
                            <p>Percentage: 83.99%</p>
                        </div>
                    </div>
                </Card.Body>
            </Card>
            <Card className={styles.education} isHoverable variant="bordered" css={{ mw: "400px" }}>
                <Card.Body>
                    <div >
                        <div className={styles.logo}>
                            <Image showSkeleton className={styles.image} objectFit="contain" css={{ height: '100px' }} src='gb.png' height="100" />
                        </div>
                        <div>
                            <p className={styles.degree}>Higher Secondary School Certificate - Science</p>
                            <p>Rosary Higher Secondary School - Goa Board</p>
                            <p>2010-2012</p>
                        </div>
                    </div>
                </Card.Body>
            </Card>
            <Card className={styles.education} isHoverable variant="bordered" css={{ mw: "400px" }}>
                <Card.Body>
                    <div>
                        <div className={styles.logo}>
                            <Image showSkeleton className={styles.image} objectFit="contain" css={{ height: '100px' }} src='gb.png' height="100" />
                        </div>
                        <div>
                            <p className={styles.degree}>Secondary School Certificate</p>
                            <p>Perpetual Succor Convent Hight School - Goa Board</p>
                            <p>Passing Year: 2010 </p>
                        </div>
                    </div>
                </Card.Body>
            </Card>
        </motion.div>
    )
}

export default Education