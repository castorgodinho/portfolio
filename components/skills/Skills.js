import { Card, Text } from '@nextui-org/react'
import Head from 'next/head';
import React from 'react'
import styles from './skills.module.css';


const Skills = () => {
    return (
        <div>
            <Head>
                <title>Technical Skills</title>
            </Head>
            <div className={styles.box}><div className={styles.number} >1</div><div className={styles.text}>Excellent in troubleshooting interface software and debugging application codes.</div></div>
            <div className={styles.box}><div className={styles.number} >2</div><div className={styles.text}>Writing application interface codes using JavaScript following react.js and react native workflows.</div></div>
            <div className={styles.box}><div className={styles.number} >3</div><div className={styles.text}>Monitoring and improving front-end performance.</div></div>
            <div className={styles.box}><div className={styles.number} >4</div><div className={styles.text}>Experience with user interface design</div></div>
            <div className={styles.box}><div className={styles.number} >5</div><div className={styles.text}>Reviewing application requirements and interface designs</div></div>
            <div className={styles.box}><div className={styles.number} >6</div><div className={styles.text}>Building reusable code and libraries for future use</div></div>
            <div className={styles.box}><div className={styles.number} >7</div><div className={styles.text}>Optimization of the application for maximum speed and scalability</div></div>
            <div className={styles.box}><div className={styles.number} >8</div><div className={styles.text}>Design and implementation of data storage solutions</div></div>
            <div className={styles.box}><div className={styles.number} >9</div><div className={styles.text}>Management of hosting environment, including database administration and scaling an application to support load changes</div></div>
            <div className={styles.box}><div className={styles.number} >10</div><div className={styles.text}>Creating database schemas that represent and support business processes</div></div>
            <div className={styles.box}><div className={styles.number} >11</div><div className={styles.text}>Proficient understanding of code versioning tool Git</div></div>
        </div>
    )
}

export default Skills