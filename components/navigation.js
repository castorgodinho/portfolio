import React, { useContext, useEffect } from 'react'
import { Navbar, Button, Link, Text } from "@nextui-org/react";
import Head from 'next/head';
import { NavigationContext } from '../contest/NavigationContext';
import { Switch, useTheme } from '@nextui-org/react'
import useDarkMode from 'use-dark-mode';
const Navigation = () => {

    const darkMode = useDarkMode(false);
    const { type, isDark } = useTheme();

    const { activeLink, setActiveLink } = useContext(NavigationContext);

    useEffect(() => {
        if (window.innerWidth < 600) {
            document.querySelector("#toggle-button").click();
        }
    }, [activeLink])

    return (
        <>
            <Head>
                <link href="https://fonts.googleapis.com/css2?family=Kaushan+Script&display=swap" rel="stylesheet" />
            </Head>
            <Navbar isCompact isBordered variant="sticky">
                <Navbar.Toggle id="toggle-button" showIn="xs" aria-label="toggle navigation" />
                <Navbar.Brand>
                    <Text b color="inherit" hideIn="xs" style={{ fontFamily: 'Kaushan Script, cursive', fontSize: '25px', color: 'brown' }}>
                        Castor Godinho
                    </Text>
                </Navbar.Brand>
                <Navbar.Collapse>
                    <Navbar.CollapseItem key={0}>
                        <Link color="inherit"
                            css={{
                                minWidth: "100%",
                            }} onPress={() => setActiveLink("about")} isActive={activeLink == "about" ? true : false} href="#">About Me</Link>
                    </Navbar.CollapseItem>
                    <Navbar.CollapseItem key={1}>
                        <Link color="inherit"
                            css={{
                                minWidth: "100%",
                            }} onPress={() => setActiveLink("education")} isActive={activeLink == "education" ? true : false} href="#">Education</Link>
                    </Navbar.CollapseItem>
                    <Navbar.CollapseItem key={2}>
                        <Link color="inherit"
                            css={{
                                minWidth: "100%",
                            }} onPress={() => setActiveLink("experience")} isActive={activeLink == "experience" ? true : false} href="#">Experience</Link>
                    </Navbar.CollapseItem>
                    <Navbar.CollapseItem key={2}>
                        <Link color="inherit"
                            css={{
                                minWidth: "100%",
                            }} onPress={() => setActiveLink("skills")} isActive={activeLink == "skills" ? true : false} href="#">Technical Skills</Link>
                    </Navbar.CollapseItem>
                    <Navbar.CollapseItem key={3}>
                        <Link color="inherit"
                            css={{
                                minWidth: "100%",
                            }} onPress={() => setActiveLink("projects")} isActive={activeLink == "projects" ? true : false} href="#">Projects</Link>
                    </Navbar.CollapseItem>
                </Navbar.Collapse>
                <Navbar.Content hideIn="xs" variant="underline">
                    <Navbar.Link onPress={() => setActiveLink("about")} isActive={activeLink == "about" ? true : false} href="#">About Me</Navbar.Link>
                    <Navbar.Link onPress={() => setActiveLink("education")} isActive={activeLink == "education" ? true : false} href="#">Education</Navbar.Link>
                    <Navbar.Link onPress={() => setActiveLink("experience")} isActive={activeLink == "experience" ? true : false} href="#">Experience</Navbar.Link>
                    <Navbar.Link onPress={() => setActiveLink("skills")} isActive={activeLink == "skills" ? true : false} href="#">Technical Skills</Navbar.Link>
                    <Navbar.Link onPress={() => setActiveLink("projects")} isActive={activeLink == "projects" ? true : false} href="#">Projects</Navbar.Link>
                </Navbar.Content>

                <Navbar.Content>
                    <Switch
                        checked={darkMode.value}
                        onChange={() => darkMode.toggle()}
                    />
                </Navbar.Content>
            </Navbar>
        </>
    )
}

export default Navigation;