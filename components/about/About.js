import { Spacer, Button } from '@nextui-org/react';
import Head from 'next/head';
import React from 'react'
import { ImQuotesLeft, ImQuotesRight } from "react-icons/im";
import { IoLogoLinkedin, IoLogoInstagram, IoLogoGoogle, IoCloudDownloadOutline } from "react-icons/io5";
import { motion } from "framer-motion";
import styles from './about.module.css'

const About = () => {
    const font = "Josefin Slab, serif"
    return (
        <motion.div initial={{
            opacity: 0
        }}
            animate={{
                opacity: 1
            }} style={{ display: 'flex', justifyContent: 'center', alignContent: 'center', alignItems: 'center', height: '80vh' }}>

            <div style={{ textAlign: 'center', }}>
                <p className={styles.title}>Full Stack Developer</p>
                <p className={styles.description}>
                    <ImQuotesLeft size="30" color='#e4e4e4' /> I am an IT professional with over 5 years of experience in analysis, design, and development using diverse technologies.
                    Seeking challenging assignments with a reputed organization to utilize acquired skills in accomplishing organizational
                    growth objectives. <ImQuotesRight size="30" color='#e4e4e4' />
                </p>
                <Spacer />
                <p><b>CONNECT WITH ME ON</b></p>
                <div style={{ margin: '10px' }}>
                    <a rel="noreferrer" href='https://www.linkedin.com/in/castor-godinho-1665577b' target="_blank" style={{ padding: '10px' }}><IoLogoLinkedin size={35} /></a>
                    <a rel="noreferrer" href='https://www.instagram.com/castor_godinho/?hl=en' target="_blank" style={{ padding: '10px' }}><IoLogoInstagram size={35} /></a>
                    <a rel="noreferrer" href='mailto: castorgodinho22@gmail.com' target="_blank" style={{ padding: '10px' }}><IoLogoGoogle size={35} /></a>
                </div>
                <p style={{ fontFamily: font, display: 'flex', justifyContent: 'center', alignItems: 'center' }}>Designed using <img style={{ padding: '5px' }} height="80" src="nextjs.png" /></p>
            </div>
        </motion.div>
    )
}

export default About