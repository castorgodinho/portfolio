export const projects = [
    {
        key: '1',
        title: 'Stock Trading Application',
        company: 'Ctrlsave Private Limited',
        timeline: '[November 2021 - Present]',
        description: 'Built as a real-world simulator game of the Indian Stock Market, this platform enables individuals to partake in and understand the working of the Stock Market without the risk of investment.',
        features: [
            'Buying and Selling Stocks',
            'Managing Holdings and Positions',
            'Distributing Stock Splits and Bonuses',
            'Creating and Managing Contests'
        ],
        responsibilites: [
            'Worked as a full stack developer',
            'Designed the system',
            'Also created some UI and UX',
            'Managed the timeline and delivery of the project'
        ],
        fontend: [
            'React',
            'React Native',
            'HTML',
            'CSS',
            'Javascript',
            'State management using ContextAPI',
        ],
        backend: [
            'Flask(Python)',
            'MySQL'
        ],
        backoffice: [
            'Yii2 Framework'
        ]
    },
    {
        key: '2',
        title: 'E-commerce Platform',
        company: 'Ctrlsave Private Limited',
        timeline: '[March 2022 - Present]',
        description: '',
        features: [
            'Platform where coffee roasters can register and sell their products',
            'Customers can register and purchase products',
            'Payment gateway, Cart, Delivery',
            'The platform also has a social media side to it',
            'Baristas can register and upload content such as recipes and blogs',
        ],
        responsibilites: [
            'Worked as a full stack developer',
            'Designed the system',
            'Also created some UI and UX',
            'Managed the timeline and delivery of the project'
        ],
        fontend: [
            'React',
            'React Native',
            'HTML',
            'CSS',
            'Javascript',
            'State management using ContextAPI',
        ],
        backend: [
            'Flask(Python)',
            'MySQL'
        ],
        backoffice: [
            'Yii2 Framework'
        ]
    },
    {
        key: '3',
        title: 'Uno Membership Application',
        company: 'Ctrlsave Private Limited',
        timeline: '[April 2021 - September 2021]',
        description: '',
        features: [
            'NFC Membership cards distributed to fuel consumers of Zamfuel Zambia',
            'These customers could use the NFC cards to buy fuel on credit',
            'The NFC card could be loaded with money',
            'The transaction history and payment history is available',
            'App allows station attendees to scan the card and issue a credit transaction'
        ],
        responsibilites: [
            'Worked as a full stack developer',
            'Designed the system',
            'Also created some UI and UX',
            'Managed the timeline and delivery of the project'
        ],
        fontend: [
            'React Native',
            'State management using ContextAPI',
        ],
        backend: [
            'Flask(Python)',
            'MySQL'
        ],
        backoffice: [
            'Yii2 Framework'
        ]
    }
] 