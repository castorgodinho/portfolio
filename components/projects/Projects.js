import React from 'react'
import { styles } from './styles'
import { Badge, Spacer } from "@nextui-org/react";
import Head from 'next/head';
import { motion } from "framer-motion";
import { projects } from './data.js'

const Projects = () => {

    return (
        <div>
            <Head>
                <title>Projects</title>
            </Head>
            <hr />
            {
                projects.map((item) => (
                    <div
                        key={item.key}
                        style={{ borderRadius: '10px', boxShadow: '1px 1px 10px -4px gray', margin: '20px 0px' }}
                    >
                        <div style={{ borderRadius: '10px 10px 0px 0px', padding: '5px' }}>
                            <p style={styles.title}>{item.title}</p>
                        </div>
                        <div style={styles.project}>
                            <p className='company-name'>{item.company}</p>
                            <p className='project-date'>{item.timeline}</p>
                            <p className='project-description'>{item.description}</p>
                            <div style={{ padding: '10px 0px', margin: '10px 0px', display: 'flex', borderTop: '1px solid #e4e4e4', borderBottom: '1px solid #e4e4e4' }}>
                                <div style={{ flex: '1' }}>
                                    <p className='features'>
                                        <b>Features</b>
                                        <ol>
                                            {
                                                item.features.map((feature, key1) => <li key={key1}>{feature}</li>)
                                            }

                                        </ol>
                                    </p>
                                </div>
                                <div style={{ flex: '1' }}>
                                    <p className='features'>
                                        <b>Responsibilities</b>
                                        <ol>
                                            {
                                                item.responsibilites.map((feature, key1) => <li key={key1}>{feature}</li>)
                                            }

                                        </ol>
                                    </p>
                                </div>
                            </div>
                            <p style={{ margin: '10px 0px' }}><b>Frontend: </b>
                                {
                                    item.fontend.map((feature, key1) => <span key={key1} style={{ borderRadius: '5px', padding: '5px', border: '1px solid brown', margin: '5px' }}>{feature}</span>)
                                }
                            </p>
                            <p style={{ margin: '10px 0px' }}><b>Backend: </b>
                                {
                                    item.backend.map((feature, key1) => <span key={key1} style={{ borderRadius: '5px', padding: '5px', border: '1px solid brown', margin: '5px' }}>{feature}</span>)
                                }
                            </p>
                            <p style={{ margin: '10px 0px' }}><b>Backoffice: </b>
                                {
                                    item.backoffice.map((feature, key1) => <span key={key1} style={{ borderRadius: '5px', padding: '5px', border: '1px solid brown', margin: '5px' }}>{feature}</span>)
                                }
                            </p>
                        </div>
                    </div>)
                )
            }


        </div>
    )
}

export default Projects;
