/* eslint-disable react/jsx-key */
import { Badge, Grid, Image } from '@nextui-org/react';
import Head from 'next/head';
import React, { useEffect, useState } from 'react'
import { motion } from "framer-motion";
import { IoCheckmarkOutline } from "react-icons/io5";
const Experience = () => {

    const [filters, setFilters] = useState(['frontend', 'library', 'tools', 'language', 'Designing', 'backend', 'framework']);

    const [selectedFilters, setSelectedFilters] = useState([])
    const tech_array = [
        {
            name: 'ReactJS',
            url: 'react.png',
            experience: '3.5 Years',
            tag: ['frontend', 'library']
        },
        {
            name: 'NextJS',
            url: 'nextjs.png',
            experience: '6 Months',
            tag: ['frontend', 'library']
        },
        {
            name: 'React Native',
            url: 'react.png',
            experience: '2 Years',
            tag: ['frontend', 'library']
        },
        {
            name: 'HTML5',
            url: 'html.png',
            experience: '5.7 Years',
            tag: ['frontend']
        },
        {
            name: 'CSS3',
            url: 'css.png',
            experience: '5.7 Years',
            tag: ['frontend']
        },
        {
            name: 'Javascript',
            url: 'js.png',
            experience: '5.7 Years',
            tag: ['frontend', 'language']
        },
        {
            name: 'jQuery',
            url: 'jquery.png',
            experience: '5.7 Years',
            tag: ['frontend', 'library']
        },
        {
            name: 'Bootstrap',
            url: 'bootstrap.png',
            experience: '5.7 Years',
            tag: ['frontend', 'library']
        },
        {
            name: 'GIT',
            url: 'git.png',
            experience: '5.7 Years',
            tag: ['tools']
        },
        {
            name: 'Digital Ocean',
            url: 'do.png',
            experience: '3 Years',
            tag: ['tools']
        },
        {
            name: 'Apache2',
            url: 'apache.png',
            experience: '5.7 Years',
            tag: ['tools']
        },
        {
            name: 'Flask',
            url: 'flask.png',
            experience: '3 Years',
            tag: ['backend', 'framework']
        },
        {
            name: 'Python',
            url: 'python.png',
            experience: '4 Years',
            tag: ['backend', 'language']
        },
        {
            name: 'Yii2 Framework',
            url: 'yii2.png',
            experience: '5.7 Years',
            tag: ['backend', 'framework']
        },
        {
            name: 'PHP',
            url: 'php.png',
            experience: '5.7 Years',
            tag: ['backend', 'language']
        },
        {
            name: 'Java',
            url: 'java.png',
            experience: '1 Years',
            tag: ['language']
        },
        {
            name: 'Java Swing',
            url: 'java.png',
            experience: '1 Years',
            tag: ['frontend']
        },
        {
            name: 'Figma',
            url: 'figma.png',
            experience: '3 Years',
            tag: ['Designing']
        },
        {
            name: 'Photostop',
            url: 'photoshop.png',
            experience: '1 Years',
            tag: ['Designing']
        },
        {
            name: 'Affinity Designer',
            url: 'designer.png',
            experience: '1 Years',
            tag: ['Designing']
        },
        {
            name: 'Affinity Photo',
            url: 'photo.png',
            experience: '2 Years',
            tag: ['Designing']
        },
        {
            name: 'MySQL',
            url: 'mysql.png',
            experience: '5.7 Years',
            tag: ['database']
        },
        {
            name: 'MySQLite',
            url: 'mysqlite.png',
            experience: '1 Years',
            tag: ['database']
        },
    ];
    const [techStack, setTechStack] = useState(tech_array);

    useEffect(() => {
        if (selectedFilters.length == 0) {
            setTechStack(tech_array);
        } else {
            const newTechStack = tech_array.filter((item, key) => {
                let flag = false;
                selectedFilters.forEach((filter, key1) => {
                    if (item.tag.includes(filter)) {
                        flag = true;
                    }
                })
                return flag ? item : null;
            });
            setTechStack(newTechStack);
        }

    }, [selectedFilters]);

    const handle_filter = (item) => {
        const found = selectedFilters.includes(item);
        if (found) {
            const newSelectedItems = selectedFilters.filter((value, key) => {
                return item != value;
            });
            setSelectedFilters(newSelectedItems);
        } else {
            setSelectedFilters([...selectedFilters, item]);
        }
    }

    return (
        <motion.div initial={{
            opacity: 0
        }}
            animate={{
                opacity: 1
            }}>
            <Head>
                <title>Experience</title>
            </Head>
            <Grid.Container gap={2}>
                {
                    filters.map((item, key) =>
                        <Grid>
                            <Badge className='filter' isSquared onClick={() => handle_filter(item)} variant="bordered" color={selectedFilters.includes(item) ? "success" : "bordered"}>{selectedFilters.includes(item) ? <IoCheckmarkOutline color="green" size={20} /> : ""}{item} </Badge>
                        </Grid>
                    )
                }
            </Grid.Container>
            <motion.div initial={{
                opacity: 0
            }}
                animate={{
                    opacity: 1
                }}>
                <Grid.Container >

                    {
                        techStack.map((item, key) =>

                            <Grid xs={6} md={2} style={{ margin: '5px 0px' }}>
                                <Grid.Container style={{ padding: '10px' }}>
                                    <Grid xs={4}>
                                        <div style={{ alignContent: 'center', padding: '5px' }}>
                                            <Image
                                                showSkeleton
                                                src={item.url}
                                            />
                                        </div>
                                    </Grid>
                                    <Grid xs={8}>
                                        <div style={{ padding: '5px' }}>
                                            <p ><span style={{ fontWeight: '700' }}>{item.name}</span> <br /> {item.experience}</p>
                                        </div>
                                    </Grid>
                                </Grid.Container>


                            </Grid>
                        )
                    }

                </Grid.Container>
            </motion.div>
            <hr />
            <div style={{ padding: '20px 0px' }}>
                <p style={{ fontSize: '16px', fontWeight: '700' }}>CtrlSave Pvt Ltd</p>
                <p style={{ fontSize: '16px', fontWeight: '700' }}>Full Stack Developer</p>
                <p>(2020 - Present) - 2 years</p>
                <p>
                    <b>Achievements and Responsibilities</b>
                    <ol>
                        <li>Maintain communication with stake holders and gather requirements</li>
                        <li>Played a critical role in managing a projects timely delivery</li>
                        <li>Was responsible to write efficient and fast REST APIs</li>
                        <li>Contributed to the UI and UX of applications. Created UI UX mockups using tools like Figma</li>
                        <li>Produced clean and efficient react code based on specifications provided</li>
                        <li>Keep Senior Management and Clients appraised of progress through detailed reports and presentations to enable effective decision making</li>
                        <li>Was part of the team to maintain and deploy applications on the cloud platform (Ubuntu Instance) along with managing the server load.</li>
                        <li>Create technical documentation for reference and reporting</li>
                        <li>Provide constructive input and suggestions to other projects in order to aid the success of other teams</li>
                    </ol>
                </p>
            </div>
            <div style={{ padding: '20px 0px' }}>
                <p style={{ fontSize: '16px', fontWeight: '700' }}>Freelance Developer</p>
                <p style={{ fontSize: '16px', fontWeight: '700' }}>Software Developer</p>
                <p>(May 2017 - Sep, 2020) - 3 years 5 Months</p>
                <p>
                    <b>Achievements and Responsibilities</b>
                    <ol>
                        <li>Meeting with clients and discussing requirements and visions</li>
                        <li>Finding the best, most effective solution to solve the problem</li>
                        <li>Worked on creating branding for businesses, developing web applications and ERP systems</li>
                        <li>Create a budget and timeline for the execution and deployment of the project</li>
                        <li>Worked with client such as PWD, Forest Department of Goa, Zamfuel (Zambia), SAFAL (Zambia) </li>
                        <li>Manage and maintain all the codebase, databases and data storage</li>
                        <li>Worked with clients on site in Zambia and Zimbabwe</li>
                        <li>Conducted on site training and systems configurations</li>
                    </ol>
                </p>
            </div>
            <div style={{ padding: '20px 0px' }}>
                <p style={{ fontSize: '16px', fontWeight: '700' }}>Parvatibai Chowgule College of Arts and Science</p>
                <p style={{ fontSize: '16px', fontWeight: '700' }}>Assitant Professor/ Coordinator</p>
                <p>(Jul, 2017 - Sep, 2020) - 3 years</p>
                <p>
                    <b>Achievements and Responsibilities</b>
                    <ol>
                        <li>Delivered lectures in Chowgule College as an Industry Expert</li>
                        <li>Coordinated the design and implementation of a new Bachelors Programme</li>
                        <li>Contributed in the budgeting and financing of the program</li>
                        <li>Delivered lectures in different colleges in Goa as a Guest Lecturer and Industry Expert</li>
                        <li>Designed and developed some software applications</li>
                        <li>Guided student in their final year projects.</li>
                    </ol>
                </p>
            </div>
        </motion.div>
    )
}

export default Experience